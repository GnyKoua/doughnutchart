import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DougntServicesService } from './lib/services/dougnt-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'donu';
  settlementValue$: Observable<any> = of()
  dougnutValue$: Observable<any> = of()
  fundsLoad: boolean = false;
  funds: any[] = [];
  currentFunds: any[] = [];
  selectedRows: any[] = [];
  showSelected: boolean = false;
  searchValue: string = '';
  previousDatas: any;

  columns = [
    { field: 'libellePortefeuille', header: 'Libelle Portefeuille' },
    { field: 'codePortefeuille', header: 'code Portefeuille' },
    { field: 'codePortefeuille', header: 'code Portefeuille' }
  ]

  @ViewChild('fundsTable') fundsTable: any
  // @ViewChild('chartTooltip',{read: ElementRef}) chartTooltipTemplate: ElementRef
  chart: any;

  constructor(
    private chartUtils: DougntServicesService,
  ) {
    //super();
  }

  count: number = 0;

  ngOnInit(): void {
    //dougnut chart
    this.settlementValue$ = of([
      {
        severity: "danger",
        status: 'failed',
        value: 122
      },
      {
        severity: "warn",
        status: 'alleged',
        value: 30
      },
      {
        severity: "matched",
        status: 'success',
        value: 50
      },
      {
        severity: "toBeRepaired",
        status: 'info',
        value: 90
      },
      {
        severity: "seltled",
        status: 'information',
        value: 90
      }
    ])

    this.dougnutValue$ = of([
      {
        title: 'circle',
        values: [1000, 13333, 8889],
        colors: [{
          backgroundColor: ['#DC2424', '#952C2C', '#491B1B'],
          hoverBackgroundColor: ['#DC2424', '#952C2C', '#491B1B']
        }],
        labels: ['reps', 'cont', 'others']
      },
    ]);
  }

  ngAfterContentInit(): void {
    if (this.previousDatas) {
      if (this.previousDatas.selectedRows) {
        this.selectedRows = this.previousDatas.selectedRows;
      }
    }
  }


  ngAfterViewInit() {
    // super.ngAfterViewInit()

    // this.options = {
    //   elements:{
    //     arc:{
    //       borderWidth: -18,
    //     }
    //   },
    //   tooltips:{
    //     enabled: false,
    //     callbacks:{
    //       label:(ttpItem: any, data: any) =>{
    //         console.log('ttttttttt',ttpItem, data )
    //       const value = data.datasets?.[ttpItem?.datasetIndex]?.data?.[ttpItem?.index];
    //       const label = data.labels?.[ttpItem?.index]
    //       if(label){
    //         return `${value}`
    //       }
    //       return 'tessssst'

    //       }
    //     },
    //    // custom: this.chartUtils.buildHTMLTooltip(this.chartTooltipTemplate)
    //   }
    // }
  }


}


