import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChartServicesService {

  constructor() { }

  public computeColorVariable = (value: string | string[]): string | any=> {
    
    if(!value){
      return '#DC4371'
    }
    if(Array.isArray(value)){
      return value.map((color)=> this.computeColorVariable(color))
    }

    if(value.indexOf('#') > -1 || value.indexOf('rgb') > -1 || !value){
      return value
    }

    const bodyStyles = window.getComputedStyle(document.body)
    const computedValue = bodyStyles.getPropertyValue(value);
    return computedValue
  }
}
