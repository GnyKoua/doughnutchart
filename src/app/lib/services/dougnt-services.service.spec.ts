import { TestBed } from '@angular/core/testing';

import { DougntServicesService } from './dougnt-services.service';

describe('DougntServicesService', () => {
  let service: DougntServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DougntServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
