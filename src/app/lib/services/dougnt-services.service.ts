import { ElementRef, Injectable } from '@angular/core';
import { Chart, ChartConfiguration, ChartDataset, TooltipItem } from 'chart.js';
import { ChartServicesService } from './chart-services.service';

export interface ChartColor {
  backgroundColor?: string[];
  hoverBackgroundColor?: string[];
  borderColor?: string[];
  hoverBorderColor?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class DougntServicesService {

  constructor(private chartUtils: ChartServicesService) { }

  public getDoughnutChartData = (dataset: number[], datasetOptions?: ChartDataset<'doughnut'> | undefined | null): ChartConfiguration<'doughnut'>['data']['datasets'] => [
    {
      ...(datasetOptions ?? {}),
      data: dataset,
    }
  ];

  public onBeforeDrawCall(
    chart: Chart,
    totalValue: number,
    unit: string,
    textFontSize?: number,
    textFont?: string,
    totalValueClick?: (item: any) => void
  ) {
    if (chart.height !== null && chart.ctx !== null && chart.width !== null) {
      const width = chart.width,
        height: number = chart.height,
        ctx = chart.ctx;
      const totalVal = unit ? `${totalValue}  ${unit}` : totalValue;
      ctx.restore();
      const fontSize = textFontSize ? textFontSize : (height / 114).toFixed(2);
      ctx.font = textFont ? textFont : fontSize + 'em sans-serif';
      ctx.textBaseline = 'middle';
      ctx.fillStyle = this.computeColorVariable('#1C1414');

      const textX = Math.round((width - ctx.measureText(totalVal.toString()).width) / 2),
        textY = height / 2;

      ctx.fillText(totalVal.toString(), textX, textY);
      const ctx2 = chart.ctx as CanvasRenderingContext2D;
      const rect = ctx2.canvas.getBoundingClientRect();

      ctx2.canvas.onclick = (event) => {
        const x2 = event.clientX - rect.left
        const y2 = event.clientY - rect.top;

        const middleX = rect.width / 2;
        const middleY = rect.height / 2;
        const thresHold = 20;

        if (Math.abs(x2 - middleX) <= thresHold && Math.abs(y2 - middleY) <= thresHold) {
          totalValueClick && totalValueClick(totalVal?.toString())
        }
      }
      ctx.save()
    }
  }

  public buildOptions(
    labelCallback: (ttpItem: TooltipItem<"doughnut">) => string | string[],
    labelTextColorCallback: (ttpItem: TooltipItem<"doughnut">) => string,
    titleCallback: (ttpItem: TooltipItem<"doughnut">[]) => string,
    additionalOptions?: ChartConfiguration<"doughnut">["options"]
  ): ChartConfiguration<"doughnut">["options"] {
    return {
      cutout: 85,
      aspectRatio: 1,
      maintainAspectRatio: true,
      elements: {
        arc: {
          borderWidth: 5,
        },
      },
      animation: {
        easing: 'easeInCubic',
        duration: 1000,
      },
      plugins: {
        legend: {
          display: false,
          position: 'right',
          onClick: this.getLegendCallBack,
        },
        tooltip: {
          enabled: true,
          callbacks: {
            label: (ttpItem: TooltipItem<"doughnut">) => labelCallback(ttpItem),
            labelTextColor: (ttpItem: TooltipItem<"doughnut">) => labelTextColorCallback(ttpItem),
            title: (ttpItem: TooltipItem<"doughnut">[]) => titleCallback(ttpItem)
          },
          backgroundColor: this.computeColorVariable('#124c66'),
          titleFont: {
            size: 12,
            style: "inherit"
          },
          titleColor: this.computeColorVariable('#ffffff'),
          titleAlign: 'center',
          bodyColor: this.computeColorVariable('#ffffff'),
          bodyAlign: 'center',
          titleMarginBottom: 8,
          caretSize: 6,
          padding: {
            x: 6,
            y: 10,
          },
          bodyFont: {
            size: 12,
          },
          borderWidth: 1,
          borderColor: this.computeColorVariable('#124c66'),
          displayColors: false,
        },
      },
      ...additionalOptions,
    }
  }

  private getLegendCallBack = (function (self: any) {
    return (chart: any) => chart.legend.legendItems
  })(this)

  public computeColorVariable = (value: string | string[]) => this.chartUtils.computeColorVariable(value)



  public buildHTMLTooltip = (tooltipElement: ElementRef) => {

    return function (this: any, tooltip: any) {
      console.log("tooltip : ", tooltip);
      const tooltipEl = tooltipElement.nativeElement;

      //Hide if no tooltip
      if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = '0';
        return;
      }

      //set caret position
      tooltipEl.classList.remove('above', 'below', 'no-transform');
      tooltipEl.classList.add(tooltip.yAlign || 'no-transform');

      //set Text
      if (tooltip.body) {
        const titleLines = tooltip.title || []
        const bodyLines = tooltip.body.map((bodyItem: any) => bodyItem.lines)

        if (!bodyLines.every((line: any) => line.length)) {
          return
        }

        let innerHtml = '<thead>'

        titleLines.forEach((title: any) => {
          innerHtml += '<tr><th>' + title + '</tr></th>'
        });

        innerHtml += '</thead><tbody>'

        bodyLines.forEach((body: any, i: any) => {
          const colors = tooltip.labelColors[i];
          let style = 'background:' + colors.backgroundColor;
          style += '; border-color:' + colors.borderColor;
          style += '; border-width: 2px';
          const span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
          innerHtml += '<tr><td>' + span + body + '</td></tr>'
        });

        innerHtml += '</tbody>';
        const tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHtml = innerHtml;
      }

      const positionY = this._chart.canvas.offsetTop
      const positionX = this._chart.canvas.offsetLeft

      const ctx2 = this._chart.ctx as CanvasRenderingContext2D;
      const rect = ctx2.canvas.getBoundingClientRect();
      ctx2.canvas.onpointerover = (event) => {
        const x2 = event.clientX - rect.left
        const y2 = event.clientY - rect.top;

        const middleX = rect.width / 2;
        const middleY = rect.height / 2;
        const thresHold = 20;

        if (Math.abs(x2 - middleX) <= thresHold && Math.abs(y2 - middleY) <= thresHold) {
          //totalValueClick && totalValueClick(totalVal?.toString())
          console.log("OK OK");
        }
      }

      console.log('positionX', positionX)
      tooltipEl.stype.opacity = '1';
      tooltipEl.stype.left = positionX + tooltip.caretX + 'px';
      tooltipEl.stype.top = positionY + tooltip.caretY + 'px';
      tooltipEl.stype.fontFamily = tooltip._bodyFontFamily;
      tooltipEl.stype.fontSize = tooltip.bodyFontSize;
      tooltipEl.stype.fontStyle = tooltip._bodyFontStyle;
      tooltipEl.stype.padding = tooltip.yPadding + 'px' + tooltip.xPadding + 'px'
    }
  }
}
