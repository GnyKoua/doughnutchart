import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, PipeTransform, SimpleChanges, ViewChild } from '@angular/core';
import { Chart, ChartConfiguration, TooltipItem } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

import { ChartColor, DougntServicesService } from '../services/dougnt-services.service';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})

export class DoughnutChartComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild(BaseChartDirective, { static: false }) baseChart: BaseChartDirective;

  public doughnutChartData: number[];
  public doughnutChartColors: ChartColor[];
  public legendColors: ChartColor[];
  private allDoughnutData: number[];
  private backupDoughnutColors: ChartColor[];
  public isLegendItemsDisabled: boolean
  datasets: ChartConfiguration<'doughnut'>['data']['datasets'] = [];

  @Input() tooltipFormatter?: PipeTransform;
  @Input() legendFormatter?: PipeTransform;
  @Input() totalFormatter?: PipeTransform;
  @Input() totalUnit: string = '';
  @Input() options: ChartConfiguration<"doughnut">["options"] = {};

  @Input() set doughnutData(value: number[]) {
    this.allDoughnutData = value || [];
    this.doughnutChartData = this.allDoughnutData;
    this.datasets = this.chartUtils.getDoughnutChartData(this.allDoughnutData, {
      data: [],
      // Colors
      ...((this.doughnutColors ?? []).length > 0 ? this.doughnutColors[0] : {}),
      // Others properties
      borderJoinStyle: this.chartBorderJoinStyle,
      borderRadius: this.borderRadius,
    });
    this.total = (<any[]>this.doughnutChartData).reduce((acc, curr) => acc + curr, 0);
    if (this.total) {
      this.isLegendItemsDisabled = false;
    } else {
      this.isLegendItemsDisabled = true;
      this.doughnutChartData = [0.000001];
    }

    if (this.baseChart && this.baseChart.chart) { }
  }

  ///
  @Input() set doughnutColors(value: ChartColor[]) {
    this.backupDoughnutColors = value;
    this.setDoughnutColors(value);
  }

  @Input('doughnutLabels') doughnutChartLabels: any[];
  @Input() tooltipLabel: string;
  @Input() set activeEntries(value: string[]) {
    this._activeEntries = value || this.doughnutChartLabels.map((elt) => elt.toString());
    this.updateChartData()
  }


  @Input() showLegend = true;
  @Input() chartBorderJoinStyle: CanvasLineJoin = "round";
  @Input() borderRadius = 10;
  @Input() doughnutTotal: number;
  @Input() showPercentInTooltip = true;
  @Input() colorInTooltipLabelText = true;
  @Input() doughnutChartDataFontSize: number;
  @Input() doughnuChartDataFont: string;

  // @Output() activeEntriesChange = new EventEmitter<string[]>();
  @Output() onDoughnutDataClick = new EventEmitter<any>();


  public doughnutChartType: any = 'doughnut';
  public doughnutChartOptions: ChartConfiguration<'doughnut'>['options'] = {};
  public doughnutChartPlugins: any[] = [
    {
      beforeDraw: (chart: Chart) =>
        this.chartUtils.onBeforeDrawCall(
          chart,
          this.totalFormatter ? this.tooltipFormatter?.transform(this.total) : this.total,
          this.totalUnit,
          this.doughnutChartDataFontSize,
          this.doughnuChartDataFont,
          (item: any) => {
            this.onDoughnutDataClick.emit(item)
          }
        )
    }
  ]


  public _activeEntries: string[];
  private total: number = 0;

  constructor(
    private chartUtils: DougntServicesService,
    private cdRef: ChangeDetectorRef
  ) { }


  ngOnInit(): void {
    this.doughnutChartOptions = this.chartUtils.buildOptions(
      this.tooltipLabelCallback,
      this.tooltipLabelTextColorCallback,
      this.tooltipTitleCallback,
      this.options
    );
  }

  private tooltipLabelCallback = (ttpItem: TooltipItem<"doughnut">) => {
    if (this.total !== 0) {
      const value = ttpItem.chart.data.datasets[ttpItem.datasetIndex]['data'][ttpItem.dataIndex];
      if (this.showPercentInTooltip) {
        return [
          `${this.legendFormatter ? this.legendFormatter.transform(value) : value} ${this.tooltipLabel ? 'this is a test' : ''
          }`,
          `${(((100 * <number>value) / this.total).toFixed(2))}%`
        ]
      }

      return [
        `${this.legendFormatter ? this.legendFormatter.transform(value) : value} ${this.tooltipLabel ? 'this is a test' : ''
        }`,
        `${(((100 * <number>value) / this.total).toFixed(2))}%`
      ]
    }
    return '0'
  }


  private tooltipLabelTextColorCallback = (ttpItem: TooltipItem<"doughnut">) => {
    if (this.total !== 0 && this.colorInTooltipLabelText) {
      const dataset = ttpItem.chart.config.data.datasets[ttpItem.datasetIndex];
      return (dataset.backgroundColor as string[])[ttpItem.dataIndex];
    }
    return "";
  }

  private tooltipTitleCallback = (ttpItem: TooltipItem<"doughnut">[]) => {
    if (this.total !== 0) {
      const item = ttpItem[0];
      const labels = item.chart.data.labels;
      return labels ? labels[item['dataIndex']] as string : ''
    }
    return '';
  }

  ngAfterViewInit() {
    this.cdRef.detectChanges()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['doughnutData'] && changes['doughnutData'].currentValue) {
      this.setDoughnutColors(this.backupDoughnutColors);
      this.datasets = this.chartUtils.getDoughnutChartData(this.allDoughnutData, {
        data: [],
        // Colors
        ...(this.doughnutChartColors.length > 0 ? this.doughnutChartColors[0] : {}),
        // Others properties
        borderJoinStyle: this.chartBorderJoinStyle,
        borderRadius: this.borderRadius,
      });
    }

    if (changes['doughnutTotal'] && (changes['doughnutTotal'].currentValue || changes['doughnutTotal'].currentValue === 0)) {

      this.doughnutChartPlugins = [
        {
          beforeDraw: (chart: any) =>
            this.chartUtils.onBeforeDrawCall(
              chart,
              changes['doughnutTotal'].currentValue,
              this.totalUnit,
              this.doughnutChartDataFontSize,
              this.doughnuChartDataFont,
              (item: any) => {
                this.onDoughnutDataClick.emit(item)
              }
            )
        }
      ]
    }
  }
  private updateChartData() {
    this.doughnutChartData = this.chartUtils.getDoughnutChartData(
      this.allDoughnutData.map((elt, index) =>
        this._activeEntries.some((entry) => this.doughnutChartLabels.indexOf(entry) === index) ? elt : 0
      )
    ).reduce((dsr: number[], ds) => {
      return [...dsr, ...ds.data];
    }, []);

    this.total = (<any[]>this.doughnutChartData).reduce((acc, curr) => acc + curr);
    this.baseChart.update()
  }


  private setDoughnutColors(value: ChartColor[]) {
    const colors = this.prepareColors(value);
    this.legendColors = colors;
    this.doughnutChartColors = this.total > 0 ? colors : [
      {
        backgroundColor: this.chartUtils.computeColorVariable('#491B1B'),
        hoverBackgroundColor: this.chartUtils.computeColorVariable('#491B1B'),
        borderColor: this.chartUtils.computeColorVariable('#491B1B'),
        hoverBorderColor: this.chartUtils.computeColorVariable('#491B1B')
      }
    ]
  }

  private prepareColors(value: ChartColor[] | undefined): ChartColor[] {
    if (!value) {
      return [];
    }

    return value.map(color => ({
      ...color,
      backgroundColor: color.backgroundColor && this.chartUtils.computeColorVariable(color.backgroundColor),
      hoverBackgroundColor: color.hoverBackgroundColor && this.chartUtils.computeColorVariable(color.hoverBackgroundColor),
      borderColor: color.borderColor && this.chartUtils.computeColorVariable(color.borderColor),
      hoverBorderColor: color.hoverBorderColor && this.chartUtils.computeColorVariable(color.hoverBorderColor)
    }));
  }
}
